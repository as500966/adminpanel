'use strict';

//dependencies
var config = require('./config/main'),
  express = require('express'),
  cookieParser = require('cookie-parser'),
  bodyParser = require('body-parser'),
  session = require('express-session'),
  mongoStore = require('connect-mongo')(session),
  http = require('http'),
  path = require('path'),
  passport = require('passport'),
  mongoose = require('mongoose'),
  helmet = require('helmet'),
  csrf = require('csurf'),
  expHbs = require('express-handlebars'),
  moment = require('moment');
  //expHbsEngine = require('./config/hbsengine');

//create express app
var app = express();

//Handlebars helpers
var hbsEngine = expHbs.create({
  defaultLayout: 'index', 
  extname: '.hbs',
  helpers: {
    //2 variables and condition Helper
    ifCond: function (v1, operator, v2, options) {
      switch (operator) {
        //{{#ifCond var1 '==' var2}}
      case '==':
        return (v1 == v2) ? options.fn(this) : options.inverse(this);
      
      //{{#ifCond var1 '===' var2}}
      case '===':
        return (v1 === v2) ? options.fn(this) : options.inverse(this);
      
      //{{#ifCond var1 '!=' var2}}
      case '!=':
        return (v1 != v2) ? options.fn(this) : options.inverse(this);
      
      //{{#ifCond var1 '!==' var2}}
      case '!==':
        return (v1 !== v2) ? options.fn(this) : options.inverse(this);
      
      //{{#ifCond var1 '<' var2}}
      case '<':
        return (v1 < v2) ? options.fn(this) : options.inverse(this);
      
      //{{#ifCond var1 '<=' var2}}
      case '<=':
        return (v1 <= v2) ? options.fn(this) : options.inverse(this);
      
      //{{#ifCond var1 '>' var2}}
      case '>':
        return (v1 > v2) ? options.fn(this) : options.inverse(this);
      
      //{{#ifCond var1 '>=' var2}}
      case '>=':
        return (v1 >= v2) ? options.fn(this) : options.inverse(this);
      
      //{{#ifCond var1 '&&' var2}}
      case '&&':
        return (v1 && v2) ? options.fn(this) : options.inverse(this);
      
      //{{#ifCond var1 '||' var2}}
      case '||':
        return (v1 || v2) ? options.fn(this) : options.inverse(this);
      
      default:
        return options.inverse(this);
      }
    },

    //format an ISO date using Moment.js
    //  moment syntax example: moment(Date("2011-07-18T15:50:52")).format("MMMM YYYY")
    // usage: {{dateFormat creation_date format="MMMM YYYY"}}
    dateFormat: function(context, block) {
      var f = block.hash.format || 'MMM DD, YYYY hh:mm:ss A';
      return moment(context).format(f); //had to remove Date(context)
    },

    //  format an ISO date using Moment.js (8.9kb min)
    //  http://momentjs.com/
    //  usage: {{dateFromNow js_date suffix=true}} removes "ago" suffix
    dateFromNow: function(context, block) {
      var s = block.hash.suffix || false;
      return moment(context).fromNow(s);
    },

    //  Function to do basic mathematical operations 
    //  Usage {{math @index "+" 1}}
    math: function(lvalue, operator, rvalue) {lvalue = parseFloat(lvalue);
      rvalue = parseFloat(rvalue);
      return {
        '+': lvalue + rvalue,
        '-': lvalue - rvalue,
        '*': lvalue * rvalue,
        '/': lvalue / rvalue,
        '%': lvalue % rvalue
      }[operator];
    },

    // Returns n-th array element from the end
    isNthLastArrayItem: function(array, index, offset, options) {

      if  ((array.length >= offset) && (array.length - offset) === index)
        return options.fn(this);
      return;
    }

    // More helpers...
  }
});

//keep reference to config
app.config = config;

//setup the web server
app.server = http.createServer(app);

//setup mongoose
mongoose.Promise = global.Promise;
app.db = mongoose.createConnection(config.mongodb.uri);
app.db.on('error', console.error.bind(console, 'mongoose connection error: '));
app.db.once('open', function () {
  //and... we have a data store
});

//config data models
require('./models/index')(app, mongoose);

//settings
app.disable('x-powered-by');
app.set('port', config.port);
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', hbsEngine.engine);
app.set('view engine', '.hbs');


//middleware
app.use(require('morgan')('dev'));
app.use(require('compression')());
app.use(require('serve-static')(path.join(__dirname, 'public')));
app.use(require('serve-static')(path.join(__dirname, 'backbone')));
app.use(require('method-override')());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser(config.cryptoKey));
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: config.cryptoKey,
  store: new mongoStore({ url: config.mongodb.uri })
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(csrf({ cookie: { signed: true } }));
helmet(app);

//response locals
app.use(function(req, res, next) {
  res.cookie('_csrfToken', req.csrfToken());
  res.locals.user = {};
  res.locals.user.defaultReturnUrl = req.user && req.user.defaultReturnUrl();
  res.locals.user.username = req.user && req.user.username;
  res.locals.user.email = req.user && req.user.email; 
  next();
});

//set account groups globally for templates
app.use(function (req, res, next) {
  if (req.user && req.user.canPlayRoleOf('account')) {
    req.app.db.models.User.findById(req.user._id)
      .populate({ path: 'roles.admin', model: 'Account', populate: { path: 'groups', select: 'name' } })
      .exec(function (err, user) {
        if (err) {
          return err;
        }
        if (user.roles.account.groups === undefined) {
          res.locals.accountGroupOfUser == '';
        } else {
          res.locals.accountGroupOfUser = user.roles.account.groups[0].name;
        }
        next();
      });
  } else {
    next();
  }
});

//set admin groups globally for templates
app.use(function (req, res, next) {
  if (req.user && req.user.canPlayRoleOf('admin')) {
    req.app.db.models.User.findById(req.user._id)
      .populate({ path: 'roles.admin', model: 'Admin', populate: { path: 'groups', select: 'name' } })
      .exec(function (err, user) {
        if (err) {
          return err;
        }
        if (user.roles.admin.groups === undefined) {
          res.locals.adminGroupOfUser == '';
        } else {
          res.locals.adminGroupOfUser = user.roles.admin.groups[0].name;
        }
        next();
      });
  } else {
    next();
  }
});

//global locals
app.locals.projectName = app.config.projectName;
app.locals.copyrightYear = new Date().getFullYear();
app.locals.contactCompany = app.config.contactCompany;
app.locals.contactPearson = app.config.contactPearson;
app.locals.contactPhone = app.config.contactPhone;
app.locals.contactAdress = app.config.contactAdress;

//setup passport
require('./config/passport')(app, passport);

//setup routes
require('./routes/routes')(app, passport);

//custom (friendly) error handler
app.use(require('./routes/http/index').http500);

//setup utilities
app.utility = {};
app.utility.sendmail = require('./util/sendmail');
app.utility.slugify = require('./util/slugify');
app.utility.workflow = require('./util/workflow');

//listen up
app.server.listen(app.config.port, function(){
  //and... we're live
  console.log(' Express server listening on port %s in %s mode.', app.get('port'), app.get('env'));
});
