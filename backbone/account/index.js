  'use strict';
  function update() {
    $('.current-date').text(moment().format('DD MMMM YYYY'));
    $('.current-time').text(moment().format('HH:mm:ss')); 
  }
  setInterval(update, 1000);