/* global app:true */


(function() {
  'use strict';

  app = app || {};


  app.User = Backbone.Model.extend({
    idAttribute: '_id',
    url: '/account/settings/'
  });

  app.Details = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      first: '',
      last: '',
      phone: '',
      company: '',
      occupaton: '',
      about: ''
    },
    url: '/account/settings/',
    parse: function(response) {
      if (response.user) {
        app.mainView.user.set(response.user);
        delete response.user;
      }

      return response;
    }
  });

  app.DetailsView = Backbone.View.extend({
    el: '#details',
    template: Handlebars.compile( $('#tmpl-details').html() ),
    events: {
      'click .btn-update': 'update'
    },
    initialize: function() {
      this.model = new app.Details();
      this.syncUp();
      this.listenTo(app.mainView.user, 'change', this.syncUp);
      this.listenTo(this.model, 'sync', this.render);
      this.render();
    },
    syncUp: function() {
      this.model.set({
        _id: app.mainView.user.id,
        first: app.mainView.user.get('name').first,
        last: app.mainView.user.get('name').last,
        phone: app.mainView.user.get('phone'),
        company: app.mainView.user.get('company'),
        occupation: app.mainView.user.get('occupation'),
        about: app.mainView.user.get('about')
      });
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));

      for (var key in this.model.attributes) {
        if (this.model.attributes.hasOwnProperty(key)) {
          this.$el.find('[name="'+ key +'"]').val(this.model.attributes[key]);
        }
      }
    },
    update: function() {
      this.model.save({
        first: this.$el.find('[name="first"]').val(),
        last: this.$el.find('[name="last"]').val(),
        phone: this.$el.find('[name="phone"]').val(),
        company: this.$el.find('[name="company"]').val(),
        occupation: this.$el.find('[name="occupation"]').val(),
        about: this.$el.find('[name="about"]').val()
      });
    }
  });

  app.MainView = Backbone.View.extend({
    el: '.page .container',
    initialize: function() {
      app.mainView = this;
      this.user = new app.User( JSON.parse( unescape($('#data-user').html()) ) );

      app.detailsView = new app.DetailsView();
    }
  });

  $(document).ready(function() {
    app.mainView = new app.MainView();
  });
}());
