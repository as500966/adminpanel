/* global app:true */

(function() {
  'use strict';

  app = app || {};

  app.User = Backbone.Model.extend({
    idAttribute: '_id',
    url: function() {
      return '/admin/users/'+ this.id +'/';
    }
  });

  app.Delete = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {}
    },
    url: function() {
      return '/admin/users/'+ app.mainView.model.id +'/';
    }
  });

  app.Identity = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      isActive: '',
      username: '',
      email: '',
      first: '',
      last: '',
      phone: '',
      company: '',
      occupaton: '',
      about: ''
    },
    url: function() {
      return '/admin/users/'+ app.mainView.model.id +'/';
    },
    parse: function(response) {
      if (response.user) {
        app.mainView.model.set(response.user);
        delete response.user;
      }

      return response;
    }
  });

  app.LinkToAccount = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      roles: {},
      newAccountId: ''
    },
    url: function() {
      return '/admin/users/'+ app.mainView.model.id +'/';
    },
    parse: function(response) {
      if (response.user) {
        app.mainView.model.set(response.user);
        delete response.user;
      }

      return response;
    }
  });

  app.LinkToAdmin = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      roles: {},
      newAdminId: ''
    },
    url: function() {
      return '/admin/users/'+ app.mainView.model.id +'/';
    },
    parse: function(response) {
      if (response.user) {
        app.mainView.model.set(response.user);
        delete response.user;
      }

      return response;
    }
  });

  app.Password = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      newPassword: '',
      confirm: ''
    },
    url: function() {
      return '/admin/users/'+ app.mainView.model.id +'/password/';
    },
    parse: function(response) {
      if (response.user) {
        app.mainView.model.set(response.user);
        delete response.user;
      }

      return response;
    }
  });

  app.Note = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      data: '',
      userCreated: {}
    },
    url: function() {
      return '/admin/users/'+ app.mainView.model.id +'/notes/'+ (this.isNew() ? '' : this.id +'/');
    },
    parse: function(response) {
      if (response.user) {
        app.mainView.model.set(response.user);
        delete response.user;
      }

      return response;
    }
  });

  app.NoteCollection = Backbone.Collection.extend({
    model: app.Note
  });

  app.Visit = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      timeVisited: '',
      userCreated: {}
    },
    url: function() {
      return '/admin/users/'+ app.mainView.model.id +'/visits/'+ (this.isNew() ? '' : this.id +'/');
    },
    parse: function(response) {
      if (response.user) {
        app.mainView.model.set(response.user);
        delete response.user;
      }

      return response;
    }
  });

  app.VisitCollection = Backbone.Collection.extend({
    model: app.Visit
  });

  app.Metadata = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      errfor: {},
      ip: '',
      agent: '',
      language: '',
      userCreated: {}
    },
    url: function() {
      return '/admin/users/'+ app.mainView.model.id +'/metadata/'+ (this.isNew() ? '' : this.id +'/');
    },
    parse: function(response) {
      if (response.user) {
        app.mainView.model.set(response.user);
        delete response.user;
      }

      return response;
    }
  });

  app.MetadataCollection = Backbone.Collection.extend({
    model: app.Metadata
  });


  app.Action = Backbone.Model.extend({
    idAttribute: '_id',
    defaults: {
      success: false,
      errors: [],
      data: '',
      userCreated: {}
    },
    url: function() {
      return '/admin/users/'+ app.mainView.model.id +'/actions/'+ (this.isNew() ? '' : this.id +'/');
    },
    parse: function(response) {
      if (response.user) {
        app.mainView.model.set(response.user);
        delete response.user;
      }

      return response;
    }
  });

  app.ActionCollection = Backbone.Collection.extend({
    model: app.Action
  });

  app.HeaderView = Backbone.View.extend({
    el: '#header',
    template: Handlebars.compile( $('#tmpl-header').html() ),
    initialize: function() {
      this.model = app.mainView.model;
      this.listenTo(this.model, 'change', this.render);
      this.render();
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));
    }
  });

  app.IdentityView = Backbone.View.extend({
    el: '#identity',
    template: Handlebars.compile( $('#tmpl-identity').html() ),
    events: {
      'click .btn-update': 'update'
    },
    initialize: function() {
      this.model = new app.Identity();
      this.syncUp();
      this.listenTo(app.mainView.model, 'change', this.syncUp);
      this.listenTo(this.model, 'sync', this.render);
      this.render();
    },
    syncUp: function() {
      this.model.set({
        _id: app.mainView.model.id,
        isActive: app.mainView.model.get('isActive'),
        username: app.mainView.model.get('username'),
        email: app.mainView.model.get('email'),
        first: app.mainView.model.get('name').first,
        last: app.mainView.model.get('name').last,
        phone: app.mainView.model.get('phone'),
        company: app.mainView.model.get('company'),
        occupation: app.mainView.model.get('occupation'),
        about: app.mainView.model.get('about')
      });
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));

      for (var key in this.model.attributes) {
        if (this.model.attributes.hasOwnProperty(key)) {
          this.$el.find('[name="'+ key +'"]').val(this.model.attributes[key]);
        }
      }
    },
    update: function() {
      this.model.save({
        isActive: this.$el.find('[name="isActive"]').val(),
        username: this.$el.find('[name="username"]').val(),
        email: this.$el.find('[name="email"]').val(),
        first: this.$el.find('[name="first"]').val(),
        last: this.$el.find('[name="last"]').val(),
        phone: this.$el.find('[name="phone"]').val(),
        company: this.$el.find('[name="company"]').val(),
        occupation: this.$el.find('[name="occupation"]').val(),
        about: this.$el.find('[name="about"]').val()

      });
    }
  });

  app.LinkToAccountView = Backbone.View.extend({
    el: '#link-account',
    template: Handlebars.compile( $('#tmpl-link-account').html() ),
    events: {
      'click .btn-account-open': 'accountOpen',
      'click .btn-account-link': 'accountLink',
      'click .btn-account-unlink': 'accountUnlink'
    },
    initialize: function() {
      this.model = new app.LinkToAccount();
      this.syncUp();
      this.listenTo(app.mainView.model, 'change', this.syncUp);
      this.listenTo(this.model, 'sync', this.render);
      this.render();
    },
    syncUp: function() {
      this.model.set({
        _id: app.mainView.model.id,
        roles: app.mainView.model.get('roles')
      });
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));

      for (var key in this.model.attributes) {
        if (this.model.attributes.hasOwnProperty(key)) {
          this.$el.find('[name="'+ key +'"]').val(this.model.attributes[key]);
        }
      }
    },
    accountOpen: function() {
      location.href = '/admin/accounts/'+ this.model.get('roles').account._id +'/';
    },
    accountLink: function() {
      this.model.save({
        newAccountId: $('[name="newAccountId"]').val()
      },{
        url: this.model.url() +'role-account/'
      });
    },
    accountUnlink: function() {
      if (confirm('Are you sure?')) {
        this.model.destroy({
          url: this.model.url() +'role-account/',
          success: function(model, response) {
            if (response.user) {
              app.mainView.model.set(response.user);
              delete response.user;
            }

            app.linkToAccountView.model.set(response);
          }
        });
      }
    }
  });

  app.LinkToAdminView = Backbone.View.extend({
    el: '#link-admin',
    template: Handlebars.compile( $('#tmpl-link-admin').html() ),
    events: {
      'click .btn-admin-open': 'adminOpen',
      'click .btn-admin-link': 'adminLink',
      'click .btn-admin-unlink': 'adminUnlink'
    },
    initialize: function() {
      this.model = new app.LinkToAdmin();
      this.syncUp();
      this.listenTo(app.mainView.model, 'change', this.syncUp);
      this.listenTo(this.model, 'sync', this.render);
      this.render();
    },
    syncUp: function() {
      this.model.set({
        _id: app.mainView.model.id,
        roles: app.mainView.model.get('roles')
      });
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));

      for (var key in this.model.attributes) {
        if (this.model.attributes.hasOwnProperty(key)) {
          this.$el.find('[name="'+ key +'"]').val(this.model.attributes[key]);
        }
      }
    },
    adminOpen: function() {
      location.href = '/admin/administrators/'+ this.model.get('roles').admin._id +'/';
    },
    adminLink: function() {
      this.model.save({
        newAdminId: $('[name="newAdminId"]').val()
      },{
        url: this.model.url() +'role-admin/'
      });
    },
    adminUnlink: function() {
      if (confirm('Are you sure?')) {
        this.model.destroy({
          url: this.model.url() +'role-admin/',
          success: function(model, response) {
            if (response.user) {
              app.mainView.model.set(response.user);
              delete response.user;
            }

            app.linkToAdminView.model.set(response);
          }
        });
      }
    },
  });

  app.PasswordView = Backbone.View.extend({
    el: '#password',
    template: Handlebars.compile( $('#tmpl-password').html() ),
    events: {
      'click .btn-password': 'password'
    },
    initialize: function() {
      this.model = new app.Password({ _id: app.mainView.model.id });
      this.listenTo(this.model, 'sync', this.render);
      this.render();
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));

      for (var key in this.model.attributes) {
        if (this.model.attributes.hasOwnProperty(key)) {
          this.$el.find('[name="'+ key +'"]').val(this.model.attributes[key]);
        }
      }
    },
    password: function() {
      this.model.save({
        newPassword: this.$el.find('[name="newPassword"]').val(),
        confirm: this.$el.find('[name="confirm"]').val()
      });
    }
  });

  app.DeleteView = Backbone.View.extend({
    el: '#delete',
    template: Handlebars.compile( $('#tmpl-delete').html() ),
    events: {
      'click .btn-delete-user': 'delete',
    },
    initialize: function() {
      this.model = new app.Delete({ _id: app.mainView.model.id });
      this.listenTo(this.model, 'sync', this.render);
      this.render();
    },
    render: function() {
      this.$el.html(this.template( this.model.attributes ));
    },
    delete: function() {
      if (confirm('Are you sure?')) {
        this.model.destroy({
          success: function(model, response) {
            if (response.success) {
              location.href = '/admin/users/';
            }
            else {
              app.deleteView.model.set(response);
            }
          }
        });
      }
    }
  });

  app.NewNoteView = Backbone.View.extend({
    el: '#notes-new',
    template: Handlebars.compile( $('#tmpl-notes-new').html() ),
    events: {
      'click .btn-add': 'addNew'
    },
    initialize: function() {
      this.model = new app.Note();
      this.listenTo(this.model, 'change', this.render);
      this.render();
    },
    render: function() {
      this.$el.html( this.template(this.model.attributes) );
    },
    validates: function() {
      var errors = [];
      if (this.$el.find('[name="data"]').val() === '') {
        errors.push('Please enter some notes.');
      }

      if (errors.length > 0) {
        this.model.set({ errors: errors });
        return false;
      }

      return true;
    },
    addNew: function() {
      if (this.validates()) {
        this.model.save({
          data: this.$el.find('[name="data"]').val()
        });
      }
    }
  });

  app.NoteCollectionView = Backbone.View.extend({
    el: '#notes-collection',
    template: Handlebars.compile( $('#tmpl-notes-collection').html() ),
    initialize: function() {
      this.collection = new app.NoteCollection();
      this.syncUp();
      this.listenTo(app.mainView.model, 'change', this.syncUp);
      this.listenTo(this.collection, 'reset', this.render);
      this.render();
    },
    syncUp: function() {
      this.collection.reset(app.mainView.model.get('notes'));
    },
    render: function() {
      this.$el.html(this.template());

      var frag = document.createDocumentFragment();
      var last = document.createTextNode('');
      frag.appendChild(last);
      this.collection.each(function(model) {
        var view = new app.NotesItemView({ model: model });
        var newEl = view.render().el;
        frag.insertBefore(newEl, last);
        last = newEl;
      }, this);
      $('#notes-items').append(frag);

      if (this.collection.length === 0) {
        $('#notes-items').append( $('#tmpl-notes-none').html() );
      }
    }
  });

  app.NotesItemView = Backbone.View.extend({
    tagName: 'div',
    className: 'note',
    template: Handlebars.compile( $('#tmpl-notes-item').html() ),
    events: {
      'click .btn-delete-note': 'delete'
    },
    render: function() {
      this.$el.html( this.template(this.model.attributes) );

      this.$el.find('.timeago').each(function(index, indexValue) {
        if (indexValue.innerText) {
          var myMoment = moment(indexValue.innerText);
          indexValue.innerText = myMoment.from();
        }
      });
      return this;
    },
    delete: function() {
      if (confirm('Are you sure?')) {
        this.model.destroy({
          success: function(model, response) {
            if (response.success) {
              location.href = '/admin/users/'+ app.mainView.model.id +'/';
            }
            else {
              app.NotesItemView.model.set(response);
            }
          }
        });
      }
    }
  });

  app.ActionCollectionView = Backbone.View.extend({
    el: '#actions-collection',
    template: Handlebars.compile( $('#tmpl-actions-collection').html() ),
    initialize: function() {
      this.collection = new app.ActionCollection();
      this.syncUp();
      this.listenTo(app.mainView.model, 'change', this.syncUp);
      this.listenTo(this.collection, 'reset', this.render);
      this.render();
    },
    syncUp: function() {
      this.collection.reset(app.mainView.model.get('actions'));
    },
    render: function() {
      this.$el.html(this.template());

      var frag = document.createDocumentFragment();
      var last = document.createTextNode('');
      frag.appendChild(last);
      this.collection.each(function(model) {
        var view = new app.ActionsItemView({ model: model });
        var newEl = view.render().el;
        frag.insertBefore(newEl, last);
        last = newEl;
      }, this);
      $('#actions-items').append(frag);

      if (this.collection.length === 0) {
        $('#actions-items').append( $('#tmpl-actions-none').html() );
      }
    }
  });

  app.ActionsItemView = Backbone.View.extend({
    tagName: 'div',
    className: 'action',
    template: Handlebars.compile( $('#tmpl-actions-item').html() ),
    render: function() {
      this.$el.html( this.template(this.model.attributes) );

      this.$el.find('.timeago').each(function(index, indexValue) {
        if (indexValue.innerText) {
          var myMoment = moment(indexValue.innerText);
          indexValue.innerText = myMoment.from();
        }
      });
      return this;
    },
  });

  app.VisitCollectionView = Backbone.View.extend({
    el: '#visits-collection',
    template: Handlebars.compile( $('#tmpl-visits-collection').html() ),
    initialize: function() {
      this.collection = new app.VisitCollection();
      this.syncUp();
      this.listenTo(app.mainView.model, 'change', this.syncUp);
      this.listenTo(this.collection, 'reset', this.render);
      this.render();
    },
    syncUp: function() {
      this.collection.reset(app.mainView.model.get('visits'));
    },
    render: function() {
      this.$el.html(this.template());

      var frag = document.createDocumentFragment();
      var last = document.createTextNode('');
      frag.appendChild(last);
      this.collection.each(function(model) {
        var view = new app.VisitsItemView({ model: model });
        var newEl = view.render().el;
        frag.insertBefore(newEl, last);
        last = newEl;
      }, this);
      $('#visits-items').append(frag);

      if (this.collection.length === 0) {
        $('#visits-items').append( $('#tmpl-visits-none').html() );
      }
    }
  });

  app.VisitsItemView = Backbone.View.extend({
    tagName: 'div',
    className: 'visit',
    template: Handlebars.compile( $('#tmpl-visits-item').html() ),
    render: function() {
      this.$el.html( this.template(this.model.attributes) );

      this.$el.find('.timeago').each(function(index, indexValue) {
        if (indexValue.innerText) {
          var myMoment = moment(indexValue.innerText);
          indexValue.innerText = myMoment.from();
        }
      });
      return this;
    },
  });

  app.MetadataCollectionView = Backbone.View.extend({
    el: '#metadatas-collection',
    template: Handlebars.compile( $('#tmpl-metadatas-collection').html() ),
    initialize: function() {
      this.collection = new app.MetadataCollection();
      this.syncUp();
      this.listenTo(app.mainView.model, 'change', this.syncUp);
      this.listenTo(this.collection, 'reset', this.render);
      this.render();
    },
    syncUp: function() {
      this.collection.reset(app.mainView.model.get('metadata'));
    },
    render: function() {
      this.$el.html(this.template());

      var frag = document.createDocumentFragment();
      var last = document.createTextNode('');
      frag.appendChild(last);
      this.collection.each(function(model) {
        var view = new app.MetadataItemView({ model: model });
        var newEl = view.render().el;
        frag.insertBefore(newEl, last);
        last = newEl;
      }, this);
      $('#metadatas-items').append(frag);

      if (this.collection.length === 0) {
        $('#metadatas-items').append( $('#tmpl-metadatas-none').html() );
      }
    }
  });

  app.MetadataItemView = Backbone.View.extend({
    tagName: 'div',
    className: 'metadata',
    template: Handlebars.compile( $('#tmpl-metadatas-item').html() ),
    render: function() {
      this.$el.html( this.template(this.model.attributes) );

      this.$el.find('.timeago').each(function(index, indexValue) {
        if (indexValue.innerText) {
          var myMoment = moment(indexValue.innerText);
          indexValue.innerText = myMoment.from();
        }
      });
      return this;
    },
  });

  app.MainView = Backbone.View.extend({
    el: '.page .container',
    initialize: function() {
      app.mainView = this;
      this.model = new app.User( JSON.parse( unescape($('#data-record').html())) );

      app.headerView = new app.HeaderView();
      app.identityView = new app.IdentityView();
      app.passwordView = new app.PasswordView();
      app.linkToAccountView = new app.LinkToAccountView();
      app.linkToAdminView = new app.LinkToAdminView();
      app.deleteView = new app.DeleteView();
      app.newNoteView = new app.NewNoteView();
      app.noteCollectionView = new app.NoteCollectionView();
      app.actionCollectionView = new app.ActionCollectionView();
      app.visitCollectionView = new app.VisitCollectionView();
      app.metadataCollectionView = new app.MetadataCollectionView();
    }
  });

  $(document).ready(function() {
    app.mainView = new app.MainView();
  });
}());