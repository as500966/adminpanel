'use strict';

exports.port = process.env.PORT || 3333;
exports.mongodb = {
  uri: process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || 'mongodb://192.168.33.10:27017/drywall'
};

//Contact (empty string if not spesified)
exports.contactCompany = 'Company1';
exports.contactPearson = 'Firstname Lastname';
exports.contactPhone = '+123456789012';
exports.contactAdress = '100 Main St PO BOX 1234 Sity ST 12345 USA';

exports.projectName = 'www.somesite.com';
exports.systemEmail = 'xa334604@gmail.com';
exports.cryptoKey = 'k3yb0ardc4t';
exports.loginAttempts = {
  forIp: 50,
  forIpAndUser: 7,
  logExpiration: '20m'
};
exports.requireAccountVerification = true; 

//set the lifetime for tracking the latest actions in admin 
exports.userActivityExpires = '5m'; 

exports.smtp = {
  from: {
    name: process.env.SMTP_FROM_NAME || exports.projectName +' Website',
    address: process.env.SMTP_FROM_ADDRESS || 'xa334604@gmail.com'
  },
  credentials: {
    user: process.env.SMTP_USERNAME || 'xa334604@gmail.com',
    password: process.env.SMTP_PASSWORD || 'as334604',
    host: process.env.SMTP_HOST || 'smtp.gmail.com',
    ssl: true
  }
};
exports.oauth = {
  twitter: {
    key: process.env.TWITTER_OAUTH_KEY || 'i3NOYZn1u8h0XjE0nEdJY20kF',
    secret: process.env.TWITTER_OAUTH_SECRET || 'Egp1740L8Pqg4qRSt8t6CVD4w1vk22JRmIRbeeQjwkVKhRyuLT'
  },
  facebook: {
    key: process.env.FACEBOOK_OAUTH_KEY || '1906560049557287',
    secret: process.env.FACEBOOK_OAUTH_SECRET || '3bd0ead05988bdb6885508fb44d75637'
  },
  github: {
    key: process.env.GITHUB_OAUTH_KEY || '',
    secret: process.env.GITHUB_OAUTH_SECRET || ''
  },
  google: {
    key: process.env.GOOGLE_OAUTH_KEY || '',
    secret: process.env.GOOGLE_OAUTH_SECRET || ''
  },
  tumblr: {
    key: process.env.TUMBLR_OAUTH_KEY || '',
    secret: process.env.TUMBLR_OAUTH_SECRET || ''
  }
};