const gulp = require('gulp');
const path = require('path');
const gulpLoadPlugins = require('gulp-load-plugins');
const $ = gulpLoadPlugins();


const concatCSS = require('gulp-concat-css');
const cocatJS = require('gulp-concat');
const mainBowerFiles = require('main-bower-files');
const runSequence = require('run-sequence');
const nodemon = require('nodemon');
const pump = require('pump');
const series = require('stream-series');

const browserSync = require('browser-sync').create();
const reload = browserSync.reload;
/*
---------------------
Paths
------------------------
*/
var paths = {
  src : {
    devenv:         ['/', '!bowercomponents','!bower_components/**/*.*', '!node_modules/**/*.*', '!test/**/*.*', '!seed/*.*', '!public/**/*.*',],
    alljs:          ['config/**/*.*', 'models/**/*.*', './routes/**/*.*', './scripts/**/*.*', 'app.js'],
    appjs:          ['./config/**/*.*', 'models/**/*.*', 'routes/**/*.*', 'app.js'],
    styles:          'styles/**/*.css',
    scripts:         'scripts/**/*.*',
    pic:             'images/**/*.*',
    testenv:         'test/spec/**/*.*',
    templates:       'views/**/*.hbs'
  },
  dist : {
    vendor:         'bowercomponents/',
    styles:         'public/css/',
    scripts:        'public/js/',
    pic:            'public/img/',
    testscripts:    'test/.tmp/'
  },
};

/*
---------------------------
Base tasks
---------------------------
*/
/* Get files from bower_components */
gulp.task('getvendor', () => {
  return gulp.src(mainBowerFiles({
    'overrides': {
      'bootstrap': {
        'main': [
          './dist/js/bootstrap.js',
          './dist/css/bootstrap.css',
          './dist/css/bootstrap-theme.css',
        ]
      },
      'jquery': {
        'main': [
          './dist/jquery.min.js'
        ]
      }
    }
  }
  ))
  .pipe(gulp.dest(paths.dist.vendor));
});
/* Build css from bower components removing unused styles */
gulp.task('buildvendoruncss', () => {
  return gulp.src(path.join(paths.dist.vendor,'*.css'))
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe(concatCSS('vendor.min.css'))
    .pipe($.uncss({html: paths.src.templates}))
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
    .pipe($.cssnano())
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(paths.dist.styles))
    .pipe(reload({stream: true}));
});
/* Build css from bower components with unused styles */
gulp.task('buildvendorcss', () => {
  return gulp.src(path.join(paths.dist.vendor,'*.css'))
    .pipe($.sourcemaps.init())
    .pipe(concatCSS('vendor.min.css'))
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
    .pipe($.cssnano())
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(paths.dist.styles))
    .pipe(reload({stream: true}));
});
/* Build javascript from bower components */
gulp.task('buildvendorjs', function(cb) {
  pump([
    series(
      gulp.src(path.join(paths.dist.vendor,'jquery.min.js')), 
      gulp.src(path.join(paths.dist.vendor,'underscore.js')), 
      gulp.src(path.join(paths.dist.vendor,'backbone.js')), 
      gulp.src(path.join(paths.dist.vendor,'bootstrap.js')), 
      gulp.src(path.join(paths.dist.vendor,'html5shiv.js')), 
      gulp.src(path.join(paths.dist.vendor,'moment.js')), 
      gulp.src(path.join(paths.dist.vendor,'respond.src.js')), 
      gulp.src(path.join(paths.dist.vendor,'handlebars.js')), 
      gulp.src(path.join(paths.dist.vendor,'jquery.cookie.js')) 
      ),
    $.sourcemaps.init(),
    cocatJS('vendor.min.js'),
    $.uglify(),
    $.sourcemaps.write(),
    gulp.dest(paths.dist.scripts),
    reload({stream: true})
  ],cb
  );
});
/* Build css from your own styles*/
gulp.task('buildmaincss', () => {
  return gulp.src(paths.src.styles)
    .pipe($.sourcemaps.init())
    .pipe(concatCSS('main.min.css'))
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
    .pipe($.cssnano())
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(paths.dist.styles))
    .pipe(reload({stream: true}));

});
/* Build your own javascript */
gulp.task('buildmainjs', function (cb) {
  pump([
    gulp.src(paths.src.scripts),
    $.sourcemaps.init(),
    cocatJS('main.min.js'),
    $.uglify(),
    $.sourcemaps.write(),
    gulp.dest(paths.dist.scripts),
    reload({stream: true})
  ], cb
  );
});
/* Mininmize images */
gulp.task('images', () => {
  return gulp.src(paths.src.pic)
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{cleanupIDs: false}] // don't remove IDs from SVGs, they are often used as hooks for embedding and styling
    })))
    .pipe(gulp.dest(paths.dist.pic))
    .pipe(reload({stream: true}));
});
/* ES6 support for the app */
gulp.task('lint', () => {
  return gulp.src(paths.src.alljs)
    .pipe(reload({stream: true, once: true}))
    .pipe($.eslint({fix: true}))
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
});
/* Cleaning before gulp tasks */
gulp.task('clean', () => {
  return gulp.src([
    paths.dist.vendor + '*.*', 
    paths.dist.pic + '*.*', 
    paths.dist.styles + '*.*', 
    paths.dist.scripts + '*.*'],
     {read: false}
     )
  .pipe($.clean());
});

/*
---------------------------
BUild
---------------------------
*/
/* BUild for production */
gulp.task('build', function(callback) {
  runSequence(
  'clean','lint','getvendor','buildvendorjs','buildmainjs','buildvendoruncss','buildmaincss','images', 
  callback);
});
/* Build for staging (adding new bower componenets or non-minimized images */
gulp.task('staging',  function(callback) {
  runSequence(
  'lint', 'clean','getvendor','buildvendorjs','buildmainjs','buildvendorcss','buildmaincss','images', 
  callback);
});


/*
-----------------------------
Testing with Mocha Chai
------------------------------
*/

gulp.task('cleanfortest', () => {
  return gulp.src([
    paths.dist.testscripts + '/**/*.*'],
     {read: false}
     )
  .pipe($.clean());
});

gulp.task('scriptsfortest', ['cleanfortest'], () => {
  return gulp.src(paths.src.scripts, {base: '.'})
    .pipe($.sourcemaps.init())
    .pipe($.babel())
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest(paths.dist.testscripts))
    .pipe(reload({stream: true}));
});

gulp.task('serve:test', ['scriptsfortest'], () => {
  browserSync.init({
    notify: false,
    port: 9999,
    ui: false,
    server: {
      baseDir: 'test',
      routes: {
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch(paths.src.scripts, ['scriptsfortest']);
  gulp.watch(['test/spec/**/*.js', paths.dist.testscripts]).on('change', reload);
});

/*----------------------------------
-----------------------------------*/

/*
--------------------------
Browser-Sync dev environment
--------------------------
*/
/* 
  Connecting guest to  borwser-sync:
    - via config.vm.network :private_network, ip: <vagrantfile ip> port: <app port>
    - default port - 3000
    - Browser connection port must be different from app working port
    - i.e. could be any port exept default (prefferable) and app port
*/

gulp.task('browser-sync', ['nodemon'], function() {
  browserSync.init(null, {
    proxy: {
      target: 'http://192.168.33.10:3333', 
      ws: true, // enables websockets
    },
    reloadDelay: 6000
  });
});

gulp.task('serve', ['browser-sync'], function () {
  gulp.watch(paths.src.appjs, {cwd: './'}).on('change', reload);
  gulp.watch(paths.src.templates, {cwd: './'}).on('change', reload);
  gulp.watch(paths.src.scripts, {cwd: './'}, ['buildmainjs']);
  gulp.watch(paths.src.styles, {cwd: './'}, ['buildmaincss']);
});


gulp.task('nodemon', function (cb) {
  var called = false;
  return nodemon({script: 'app.js', legacyWatch: true}).on('start', function () {
    if (!called) {
      called = true;
      cb();
    }
  });
});

/*----------------------------------
-----------------------------------*/

