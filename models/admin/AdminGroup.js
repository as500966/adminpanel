'use strict';

exports = module.exports = function(app, mongoose) {
  var adminGroupSchema = new mongoose.Schema({
    _id: { type: String, default: '' },
    name: { type: String, default: '' },
    permissions: [{ 
      name: String,
      permit: Boolean 
    }]
  });
  adminGroupSchema.plugin(require('./plugins/pagedFind'));
  adminGroupSchema.set('autoIndex', (app.get('env') === 'development'));
  app.db.model('AdminGroup', adminGroupSchema);
};
