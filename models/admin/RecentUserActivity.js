'use strict';

exports = module.exports = function(app, mongoose) {
  var recentUserActivitySchema = new mongoose.Schema({
    action: { type: String, default: '' },
    username: { type: String, default: '' },
    email: { type: String, default: '' },
    name: { type: String, default: '' },
    isActive: String,
    valid: {type: Date, default: Date.now(), expires: app.config.userActivityExpires}
  });
  app.db.model('RecentUserActivity', recentUserActivitySchema);
};
