'use strict';

exports = module.exports = function(app, mongoose) {
  var userMetaDataSchema = new mongoose.Schema({

    ip: { type: String, default: '' },
    agent: { type: String, default: '' },
    language: { type: String, default: '' },
    userCreated: {
      id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      name: { type: String, default: '' },
    }
  });
  app.db.model('UserMetaData', userMetaDataSchema);
};

