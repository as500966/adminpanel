'use strict';

exports = module.exports = function(app, mongoose) {
  var userVisitSchema = new mongoose.Schema({
    timeVisited: { type: Date, default: '' },
    userCreated: {
      id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      name: { type: String, default: '' },
    }
  });
  app.db.model('UserVisit', userVisitSchema);
};
