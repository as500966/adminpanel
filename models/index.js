'use strict';

exports = module.exports = function(app, mongoose) {
  //embeddable docs first
  require('./admin/UserNote')(app, mongoose);
  require('./admin/UserVisit')(app, mongoose);
  require('./admin/UserAction')(app, mongoose);
  require('./admin/UserMetaData')(app, mongoose);
  require('./admin/RecentUserActivity')(app, mongoose);
  require('./admin/Category')(app, mongoose);
  require('./admin/Status')(app, mongoose);
  require('./admin/StatusLog')(app, mongoose);

  //then regular docs
  require('./admin/User')(app, mongoose);
  require('./admin/Admin')(app, mongoose);
  require('./admin/AdminGroup')(app, mongoose);
  require('./admin/AccountGroup')(app, mongoose);
  require('./admin/Account')(app, mongoose);
  require('./admin/LoginAttempt')(app, mongoose);
};

