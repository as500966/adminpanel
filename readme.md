readme here


## Настройка

- Определить название виртуальной машины
- Количество витрульной памяти для ВМ
- Название синхронизированных папок
- Запустить установку (vagrant up)
- Обновить ОС - 'vagrant box update'

## Настройка mongodb
- Зайти в главную папку через `cd /` 
- Создать папки `data/db` через `sudo `
- Зайти в директорию etc, открыть файл `mongod.conf` и изменить путь к хранению данных на `/data/db`, а также изменить адрес на тот на котором работает вагрант машина 
- Переписать права на папки через ``sudo chown -R `id -u` /data/db``
- Убрать предупреждеине `transparent_hugepage/defrag` Открыть файл `/etc/init/mongod.conf`. После строки chown `$DAEMONUSER /var/run/mongodb.pid` Добавить код:
```
if test -f /sys/kernel/mm/transparent_hugepage/enabled; then
   echo never > /sys/kernel/mm/transparent_hugepage/enabled
fi
if test -f /sys/kernel/mm/transparent_hugepage/defrag; then
   echo never > /sys/kernel/mm/transparent_hugepage/defrag
fi
```
- Презагрузить настройки сервиса: `sudo service mongod restart`
- Запустить 'mongod', проверить, что не выдало никаких ошибок


## Сборка

- Сборка осуществляется через Gulp.
- Все vendor библиотеки конкатинируются и минифицируются и могут работать в офлайн, кроме font awesome.
- Синхронизация с браузерами работает через browser-sync через ip ВМ - "192.168.33.10" на порту по умолчанию - 3000.
- Тестировочный сервер для mocha и chai работает на localhost порте 9999
- Код для теста и для сервера можно писать и в ES6 и в ES5, оба окружения компилируются в ES5 перед запуском.
- Для всего приложения существует поддержка синтаксиса ES6 через ESlint.

### Работа

- `vagrant up` - запускает ВМ.
- `mongod` - запускает сервер MongoDB.
- `gulp staging` - производит сборку css и js без удаления стилей (uncss), предназначается для разработки.
- `gulp serve` - запускает сервер синхронизированный через browser-sync, компилирует при изменении собственные стили и скрипты, не включая сторонние библиотеки.
- `gulp serve:test` - запускает сервер для тестирования с mocha chai

### Инициализация

- Создать пользователя **Root Admin**, изменив адрес почты на нужный
```javascript
db.admingroups.insert({ _id: 'root', name: 'Root' });
db.admins.insert({ name: {first: 'Root', last: 'Admin', full: 'Root Admin'}, groups: ['root'] });
var rootAdmin = db.admins.findOne();
db.users.save({ username: 'root', isActive: 'yes', email: 'xa334604@gmail.com', roles: {admin: rootAdmin._id} });
var rootUser = db.users.findOne();
rootAdmin.user = { id: rootUser._id, name: rootUser.username };
db.admins.save(rootAdmin);
```
### Инициализация дополнительного пользователя
```javascript
use drywall;
db.admingroups.insert({ _id: 'root2', name: 'Root2' });
db.admingroups.insert({ _id: 'admins2', name: 'Admins2' });
db.admingroups.insert({ _id: 'moderators2', name: 'Moderators2' });
db.accountgroups.insert({ _id: 'visitors2', name: 'Visitors2' });
db.accountgroups.insert({ _id: 'customers2', name: 'Customers2' });
db.admins.insert({ name: {first: 'Root2', last: 'Admin2', full: 'Root2 Admin2'}, groups: ['root2'] });
db.accounts.insert({ name: {first: 'Root2', last: 'Admin2', full: 'Root2 Admin2'}, groups: ['visitors2'] });
var rootAdmin2 = db.admins.findOne({ name: {first: 'Root2', last: 'Admin2', full: 'Root2 Admin2'}, groups: ['root2'] });
var rootAccount2 = db.accounts.findOne({ name: {first: 'Root2', last: 'Admin2', full: 'Root2 Admin2'}, groups: ['visitors2'] });
db.users.save({ username: 'root2', isActive: 'yes', email: 'sitetesting01@mail.ru', roles: {admin: rootAdmin2._id, account: rootAccount2._id} });
var rootUser2 = db.users.findOne({ username: 'root2'});
rootAdmin2.user = { id: rootUser2._id, name: rootUser2.username };
db.admins.save(rootAdmin2);
rootAccount2.user = { id: rootUser2._id, name: rootUser2.username };
db.accounts.save(rootAccount2);
```

### Готовая инициализация для новой версии (через username)
```javascript
use drywall;
db.admingroups.insert({ _id: 'root', name: 'Root' });
db.admingroups.insert({ _id: 'admins', name: 'Admins' });
db.admingroups.insert({ _id: 'moderators', name: 'Moderators' });
db.accountgroups.insert({ _id: 'visitors', name: 'Visitors' });
db.accountgroups.insert({ _id: 'customers', name: 'Customers' });
db.admins.insert({ name: {first: 'Root', last: 'Admin', full: 'Root Admin'}, groups: ['root'] });
db.accounts.insert({ name: {first: 'Root', last: 'Admin', full: 'Root Admin'}, groups: ['visitors'] });
var rootAdmin = db.admins.findOne();
var rootAccount = db.accounts.findOne();
db.users.save({ username: 'root', isActive: 'yes', email: 'xa334604@gmail.com', roles: {admin: rootAdmin._id, account: rootAccount._id} });
var rootUser = db.users.findOne();
rootAdmin.user = { id: rootUser._id, name: rootUser.username };
db.admins.save(rootAdmin);
rootAccount.user = { id: rootUser._id, name: rootUser.username };
db.accounts.save(rootAccount);
```

- Список коллекций в db Drywall
    accounts
    admingroups
    admins
    categories
    loginattempts
    sessions
    status
    system.indexes