'use strict';

var renderSettings = function(req, res, next) {
  var outcome = {};
  var getUserData = function(callback) {
    req.app.db.models.User.findById(req.user.id)
    .populate('roles.admin')
    .populate('roles.account')
    .exec(function (err, user) {
      if (err) {
        callback(err, null);
      }
  
      outcome.user = user;
      return callback(null, 'done');
    });
  };

  var asyncFinally = function(err, user) {
    if (err) {
      return next(err);
    }

    res.render('account/index', {
      user: outcome.user
    });
  };

  require('async').parallel( [getUserData], asyncFinally);
};

exports.init = function(req, res, next){
  renderSettings(req, res, next, '');
};