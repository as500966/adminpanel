'use strict';

var renderSettings = function(req, res, next, oauthMessage) {
  var outcome = {};

  var getUserData = function(callback) {
    var fieldsToSet = 'name phone company occupation about twitter.id facebook.id google.id';

    req.app.db.models.User.findById(req.user.id, fieldsToSet).exec(function(err, user) {
      if (err) {
        callback(err, null);
      }

      outcome.user = user;
      return callback(null, 'done');
    });
  };

  var asyncFinally = function(err, results) {
    if (err) {
      return next(err);
    }

    res.render('account/settings/index', {
      data: {
        user: escape(JSON.stringify(outcome.user))
      },
      oauthMessage: oauthMessage,
      oauthTwitter: !!req.app.config.oauth.twitter.key,
      oauthTwitterActive: outcome.user.twitter ? !!outcome.user.twitter.id : false,
      oauthFacebook: !!req.app.config.oauth.facebook.key,
      oauthFacebookActive: outcome.user.facebook ? !!outcome.user.facebook.id : false,
      oauthGoogle: !!req.app.config.oauth.google.key,
      oauthGoogleActive: outcome.user.google ? !!outcome.user.google.id : false,
    });
  };

  require('async').parallel([getUserData], asyncFinally);
};

exports.init = function(req, res, next){
  renderSettings(req, res, next, '');
};

exports.connectTwitter = function(req, res, next){
  req._passport.instance.authenticate('twitter', function(err, user, info) {
    if (!info || !info.profile) {
      return res.redirect('/account/settings/');
    }

    req.app.db.models.User.findOne({ 'twitter.id': info.profile.id, _id: { $ne: req.user.id } }, function(err, user) {
      if (err) {
        return next(err);
      }

      if (user) {
        renderSettings(req, res, next, 'Another user has already connected with that Twitter account.');
      }
      else {
        var fieldsToSet = {
          'twitter.id': info.profile.id
        };
        var options = { new: true };
        req.app.db.models.User.findByIdAndUpdate(req.user.id, fieldsToSet, options, function(err, user) {
          if (err) {
            return next(err);
          }

          res.redirect('/account/settings/');
        });
      }
    });
  })(req, res, next);
};


exports.connectFacebook = function(req, res, next){
  req._passport.instance.authenticate('facebook', { callbackURL: '/account/settings/facebook/callback/' }, function(err, user, info) {
    if (!info || !info.profile) {
      return res.redirect('/account/settings/');
    }

    req.app.db.models.User.findOne({ 'facebook.id': info.profile.id, _id: { $ne: req.user.id } }, function(err, user) {
      if (err) {
        return next(err);
      }

      if (user) {
        renderSettings(req, res, next, 'Another user has already connected with that Facebook account.');
      }
      else {
        var fieldsToSet = {
          'facebook.id': info.profile.id
        };
        var options = { new: true };
        req.app.db.models.User.findByIdAndUpdate(req.user.id, fieldsToSet, options, function(err, user) {
          if (err) {
            return next(err);
          }

          res.redirect('/account/settings/');
        });
      }
    });
  })(req, res, next);
};

exports.connectGoogle = function(req, res, next){
  req._passport.instance.authenticate('google', { callbackURL: '/account/settings/google/callback/' }, function(err, user, info) {
    if (!info || !info.profile) {
      return res.redirect('/account/settings/');
    }

    req.app.db.models.User.findOne({ 'google.id': info.profile.id, _id: { $ne: req.user.id } }, function(err, user) {
      if (err) {
        return next(err);
      }

      if (user) {
        renderSettings(req, res, next, 'Another user has already connected with that Google account.');
      }
      else {
        var fieldsToSet = {
          'google.id': info.profile.id
        };
        var options = { new: true };
        req.app.db.models.User.findByIdAndUpdate(req.user.id, fieldsToSet, options, function(err, user) {
          if (err) {
            return next(err);
          }

          res.redirect('/account/settings/');
        });
      }
    });
  })(req, res, next);
};



exports.disconnectTwitter = function(req, res, next){
  var fieldsToSet = {
    twitter: {
      id: undefined
    }
  };
  var options = { new: true };
  req.app.db.models.User.findByIdAndUpdate(req.user.id, fieldsToSet, options, function(err, user) {
    if (err) {
      return next(err);
    }

    res.redirect('/account/settings/');
  });
};


exports.disconnectFacebook = function(req, res, next){
  var fieldsToSet = {
    facebook: {
      id: undefined
    }
  };
  var options = { new: true };
  req.app.db.models.User.findByIdAndUpdate(req.user.id, fieldsToSet, options, function(err, user) {
    if (err) {
      return next(err);
    }

    res.redirect('/account/settings/');
  });
};

exports.disconnectGoogle = function(req, res, next){
  var fieldsToSet = {
    google: {
      id: undefined
    }
  };
  var options = { new: true };
  req.app.db.models.User.findByIdAndUpdate(req.user.id, fieldsToSet, options, function(err, user) {
    if (err) {
      return next(err);
    }

    res.redirect('/account/settings/');
  });
};


exports.details = function(req, res, next){
  var workflow = req.app.utility.workflow(req, res);

  workflow.on('validate', function() {
    if (!req.body.first) {
      workflow.outcome.errfor.first = 'Required';
    } else if (req.body.first.length > 15) {
      workflow.outcome.errfor.first = 'The name is too long';
    }

    if (!req.body.last) {
      workflow.outcome.errfor.last = 'Required';
    } else if (req.body.last.length > 15) {
      workflow.outcome.errfor.last = 'The name is too long';
    }

    if (req.body.phone.length !== 0){
      if (!/^\+[0-9]{12}$/.test(req.body.phone)) {
        workflow.outcome.errfor.phone = 'intl format required +XXXXXXXXXXXX'; 
      }
    }

    if (req.body.company.length !== 0){
      if (req.body.company.length > 15) {
        workflow.outcome.errfor.company = 'The name is too long'; 
      }
    }

    if (req.body.occupation.length !== 0){
      if (req.body.occupation.length > 15) {
        workflow.outcome.errfor.occupation = 'The decription is too long'; 
      }
    }

    if (req.body.about.length !== 0){
      if (req.body.about.length > 2000) {
        workflow.outcome.errfor.about = 'The text is too long'; 
      }
    }
    else if (req.body.about && typeof req.body.about !== 'string') {
      workflow.outcome.errfor.about = 'required field must be a string';
    }

    if (workflow.hasErrors()) {
      return workflow.emit('response');
    }

    workflow.emit('patchUserData');
  });

  workflow.on('patchUserData', function() {
    var fieldsToSet = {
      name: {
        first: req.body.first,
        last: req.body.last,
        full: req.body.first +' '+ req.body.last,
      },
      phone: req.body.phone,
      company: req.body.company,
      occupation: req.body.occupation,
      about: req.body.about,
      search: [
        req.body.first,
        req.body.last,
        req.body.phone,
        req.body.company,
        req.body.occupation,
        req.body.about
      ]
    };
    var options = {
      select: 'name.first name.last phone company occupation about',
      new: true
    };
    req.app.db.models.User.findByIdAndUpdate(req.user.id, fieldsToSet, options, function(err, user) {
      if (err) {
        return workflow.emit('exception', err);
      }

      workflow.outcome.user = user;
      workflow.emit('addAction');
    });

  });

  workflow.on('addAction', function() {
    var fieldsToSet = {
      $push: {
        actions: {
          data: 'User '+ req.user.username +' has updated personal info on his account page',
          userCreated: {
            id: req.user._id,
            name: req.user.username,
            time: new Date().toISOString()
          }
        }
      }
    };
    var options = { new: true };
    req.app.db.models.User.findByIdAndUpdate(req.user.id, fieldsToSet, options, function(err, user) {
      if (err) {
        return workflow.emit('exception', err);
      }

      workflow.outcome.user = user;
      return workflow.emit('response');
    });
  });

  workflow.emit('validate');
};


