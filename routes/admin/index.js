'use strict';

exports.init = function(req, res, next){
  var outcome = [];

  var countUsers = function(callback) {
    req.app.db.models.User.count({}).exec(function(err, userCount) {
      if (err) { return callback(err, null); }
      outcome.userCount = userCount;
      return callback(null, 'done');
    });
  };
  var countInactiveUsers = function(callback) {
    req.app.db.models.User.count( { isActive: 'no'} ).exec(function(err, userInactiveCount) {
      if (err) { return callback(err, null); }
      outcome.userInactiveCount = userInactiveCount;
      return callback(null, 'done');
    });
  };
  var countVisitorsForUser = function(callback) {
    req.app.db.models.Account.find({ 'user.id': { '$exists': true, '$ne': null }}, {groups: ['visitors'] }).count().exec(function(err, visitorsForUserCount) {
      if (err) { return callback(err, null); }
      outcome.visitorsForUserCount = visitorsForUserCount;
      return callback(null, 'done');
    });
  };
  var countCustomersForUser = function(callback) {
    req.app.db.models.Account.find({ 'user.id': { '$exists': true, '$ne': null }}, {groups: ['customers'] }).count().exec(function(err, customersForUserCount) {
      if (err) { return callback(err, null); }
      outcome.customersForUserCount = customersForUserCount;
      return callback(null, 'done');
    });
  };
  var countNoAccountLink = function(callback) {
    req.app.db.models.User.count({ 'roles.account': { '$exists': false } }).exec(function(err, noAccountLinkCount) {
      if (err) { return callback(err, null); }
      outcome.noAccountLinkCount = noAccountLinkCount;
      return callback(null, 'done');
    });
  };
  var countAccounts = function(callback) {
    req.app.db.models.Account.count({}).exec(function(err, accountCount) {
      if (err) { return callback(err, null); }
      outcome.accountCount = accountCount;
      return callback(null, 'done');
    });
  };
  var countVisitorsForAccount = function(callback) {
    req.app.db.models.Account.find({ groups: ['visitors'] }).count().exec(function(err, visitorsForAccountCount) {
      if (err) { return callback(err, null); }
      outcome.visitorsForAccountCount = visitorsForAccountCount;
      return callback(null, 'done');
    });
  };
  var countCustomersForAccount = function(callback) {
    req.app.db.models.Account.find({ groups: ['customers'] }).count().exec(function(err, customersForAccountCount) {
      if (err) { return callback(err, null); }
      outcome.customersForAccountCount = customersForAccountCount;
      return callback(null, 'done');
    });
  };
  var countNotVerifiedAccounts = function(callback) {
    req.app.db.models.Account.find({ $or: [{'isVerified': 'no'}, {'isVerified': {'$eq': ''} }] }).count().exec(function(err, notVerifiedAccountsCount) {
      if (err) { return callback(err, null); }
      outcome.notVerifiedAccountsCount = notVerifiedAccountsCount;
      return callback(null, 'done');
    });
  };
  var countNoUserLinkInAccount = function(callback) {
    req.app.db.models.Account.find({ $or: [{'user.id': null}, {'user.id': {'$exists': false} }] }).count().exec(function(err, noUserLinkInAccountCount) {
      if (err) { return callback(err, null); }
      outcome.noUserLinkInAccountCount = noUserLinkInAccountCount;
      return callback(null, 'done');
    });
  };
  var countAdmins = function(callback) {
    req.app.db.models.Admin.count({}).exec(function(err, adminCount) {
      if (err) { return callback(err, null); }
      outcome.adminCount = adminCount;
      return callback(null, 'done');
    });
  };
  var countRootsForAdmin = function(callback) {
    req.app.db.models.Admin.find({ groups: ['root'] }).count().exec(function(err, rootsForAdminCount) {
      if (err) { return callback(err, null); }
      outcome.rootsForAdminCount = rootsForAdminCount;
      return callback(null, 'done');
    });
  };
  var countAdministratorsForAdmin = function(callback) {
    req.app.db.models.Admin.find({ groups: ['admins'] }).count().exec(function(err, administratorsForAdminCount) {
      if (err) { return callback(err, null); }
      outcome.administratorsForAdminCount = administratorsForAdminCount;
      return callback(null, 'done');
    });
  };
  var countModeratorsForAdmin = function(callback) {
    req.app.db.models.Admin.find({ groups: ['moderators'] }).count().exec(function(err, moderatorsForAdminCount) {
      if (err) { return callback(err, null); }
      outcome.moderatorsForAdminCount = moderatorsForAdminCount;
      return callback(null, 'done');
    });
  };
  var countNoUserLinkInAdmin = function(callback) {
    req.app.db.models.Admin.find({ $or: [{'user.id': null}, {'user.id': {'$exists': false} }] }).count().exec(function(err, noUserLinkInAdminCount) {
      if (err) { return callback(err, null); }
      outcome.noUserLinkInAdminCount = noUserLinkInAdminCount;
      return callback(null, 'done');
    });
  };
  var countAccountGroups = function(callback) {
    req.app.db.models.AccountGroup.count({}).exec(function(err, accountGroupCount) {
      if (err) { return callback(err, null); }
      outcome.accountGroupCount = accountGroupCount;
      return callback(null, 'done');
    });
  };
  var countAdminGroups = function(callback) {
    req.app.db.models.AdminGroup.count({}).exec(function(err, adminGroupCount) {
      if (err) { return callback(err, null); }
      outcome.adminGroupCount = adminGroupCount;
      return callback(null, 'done');
    });
  };




  var asyncFinally = function(err, results) {
    if (err) {
      return next(err);
    }

    res.render('admin/index', {
      userCount: outcome.userCount,
      userInactiveCount: outcome.userInactiveCount,
      visitorsForUserCount: outcome.visitorsForUserCount,
      customersForUserCount: outcome.customersForUserCount,
      noAccountLinkCount: outcome.noAccountLinkCount,
      accountCount: outcome.accountCount,
      visitorsForAccountCount: outcome.visitorsForAccountCount,
      customersForAccountCount: outcome.customersForAccountCount,
      notVerifiedAccountsCount: outcome.notVerifiedAccountsCount,
      noUserLinkInAccountCount: outcome.noUserLinkInAccountCount,
      adminCount: outcome.adminCount,
      rootsForAdminCount: outcome.rootsForAdminCount,
      administratorsForAdminCount: outcome.administratorsForAdminCount,
      moderatorsForAdminCount: outcome.moderatorsForAdminCount,
      noUserLinkInAdminCount: outcome.noUserLinkInAdminCount,
      accountGroupCount: outcome.accountGroupCount,
      adminGroupCount: outcome.adminGroupCount
    });
  };

  require('async').parallel([
    countUsers, countInactiveUsers, countVisitorsForUser, countCustomersForUser, countNoAccountLink,
    countAccounts, countVisitorsForAccount, countCustomersForAccount, countNotVerifiedAccounts, countNoUserLinkInAccount,
    countAdmins, countRootsForAdmin, countAdministratorsForAdmin, countModeratorsForAdmin, countNoUserLinkInAdmin,
    countAccountGroups, countAdminGroups
  ], asyncFinally);
};

exports.activity = function(req, res, next){
  req.app.db.models.RecentUserActivity.find().exec(function(err, recentuserActivities) {
    if (err) {
      return next(err);
    }

    if (req.xhr) {
      console.log('fdgadgs', recentuserActivities);
      res.send(recentuserActivities);
    }
    else {
      res.render('admin/index', { data: { record: escape(JSON.stringify(recentuserActivities)) } });
    }
    
  });
};

exports.find = function(req, res, next){
  req.query.q = req.query.q ? req.query.q : '';
  var regexQuery = new RegExp('^.*?'+ req.query.q +'.*$', 'i');
  var outcome = {};

  var searchUsers = function(done) {
    req.app.db.models.User.find({search: regexQuery}, 'username').sort('username').limit(10).lean().exec(function(err, results) {
      if (err) {
        return done(err, null);
      }

      outcome.users = results;
      done(null, 'searchUsers');
    });
  };

  var searchAccounts = function(done) {
    req.app.db.models.Account.find({search: regexQuery}, 'name.full').sort('name.full').limit(10).lean().exec(function(err, results) {
      if (err) {
        return done(err, null);
      }

      outcome.accounts = results;
      return done(null, 'searchAccounts');
    });
  };

  var searchAdministrators = function(done) {
    req.app.db.models.Admin.find({search: regexQuery}, 'name.full').sort('name.full').limit(10).lean().exec(function(err, results) {
      if (err) {
        return done(err, null);
      }

      outcome.administrators = results;
      return done(null, 'searchAdministrators');
    });
  };

  var asyncFinally = function(err, results) {
    if (err) {
      return next(err, null);
    }

    res.send(outcome);
  };

  require('async').parallel([searchUsers, searchAccounts, searchAdministrators], asyncFinally);
};