'use strict';

exports.init = function(req, res){
  res.render('contact/index');
};

exports.sendMessage = function(req, res){
  var workflow = req.app.utility.workflow(req, res);

  workflow.on('validate', function() {
    if (!req.body.name) {
      workflow.outcome.errfor.name = 'required';
    }
    else if (req.body.name && typeof req.body.name !== 'string') {
      workflow.outcome.errfor.name = 'required field must be a string';
    }

    if (!req.body.email) {
      workflow.outcome.errfor.email = 'required';
    }
    else if (!/^[a-zA-Z0-9\-\_\.\+]+@[a-zA-Z0-9\-\_\.]+\.[a-zA-Z0-9\-\_]+$/.test(req.body.email)) {
      workflow.outcome.errfor.email = 'invalid email format';
    }

    if (!req.body.message) {
      workflow.outcome.errfor.message = 'required';
    }
    else if (req.body.message.length > 2000) {
      workflow.outcome.errfor.message = 'The text is too long'; 
    }
    else if (req.body.message && typeof req.body.message !== 'string') {
      workflow.outcome.errfor.message = 'required field must be a string';
    }

    if (workflow.hasErrors()) {
      return workflow.emit('response');
    }

    workflow.emit('sendEmail');
  });

  workflow.on('sendEmail', function() {
    req.app.utility.sendmail(req, res, {
      from: req.app.config.smtp.from.name +' <'+ req.app.config.smtp.from.address +'>',
      replyTo: req.body.email,
      to: req.app.config.systemEmail,
      subject: req.app.config.projectName +' contact form',
      textPath: 'contact/email-text',
      htmlPath: 'contact/email-html',
      locals: {
        name: req.body.name,
        email: req.body.email,
        message: req.body.message,
        projectName: req.app.config.projectName
      },
      success: function(message) {
        workflow.emit('addAction');
      },
      error: function(err) {
        workflow.outcome.errors.push('Error Sending: '+ err);
        workflow.emit('response');
      }
    });
  });

  workflow.on('addAction', function() {
    var conditions = { email: req.body.email.toLowerCase() };
    var fieldsToSet = {
      $push: {
        actions: {
          data: 'User sent a message from contact form: \n' + req.body.message,
          userCreated: {
            id: null,
            name: null,
            time: new Date().toISOString()
          }
        }
      }
    };
    var options = { new: true };
    req.app.db.models.User.findOneAndUpdate(conditions, fieldsToSet, options, function(err, user) {
      if (err) {
        return workflow.emit('exception', err);
      }

      workflow.outcome.user = user;
      return workflow.emit('response');
    });
  });

  workflow.emit('validate');
};

