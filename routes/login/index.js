'use strict';

var getReturnUrl = function(req) {
  var returnUrl = req.user.defaultReturnUrl();
  if (req.session.returnUrl) {
    returnUrl = req.session.returnUrl;
    delete req.session.returnUrl;
  }
  return returnUrl;
};

exports.init = function(req, res){
  if (req.isAuthenticated()) {
    res.redirect(getReturnUrl(req));
  }
  else {
    res.render('login/index', {
      oauthMessage: '',
      oauthTwitter: !!req.app.config.oauth.twitter.key,
      oauthFacebook: !!req.app.config.oauth.facebook.key,
      oauthGoogle: !!req.app.config.oauth.google.key
    });
  }
};

exports.login = function(req, res){
  var workflow = req.app.utility.workflow(req, res);

  workflow.on('validate', function() {
    if (!req.body.username) {
      workflow.outcome.errfor.username = 'required';
    }
    else if (req.body.username && typeof req.body.username !== 'string') {
      workflow.outcome.errfor.username = 'required field must be a string';
    }

    if (req.body.password.length < 8) {
      workflow.outcome.errfor.password = 'must be at least 8 characters';
    }

    if (workflow.hasErrors()) {
      return workflow.emit('response');
    }

    workflow.emit('abuseFilter');
  });

  workflow.on('abuseFilter', function() {
    var getIpCount = function(done) {
      var conditions = { ip: req.ip };
      req.app.db.models.LoginAttempt.count(conditions, function(err, count) {
        if (err) {
          return done(err);
        }

        done(null, count);
      });
    };

    var getIpUserCount = function(done) {
      var conditions = { ip: req.ip, user: req.body.username };
      req.app.db.models.LoginAttempt.count(conditions, function(err, count) {
        if (err) {
          return done(err);
        }

        done(null, count);
      });
    };

    var asyncFinally = function(err, results) {
      if (err) {
        return workflow.emit('exception', err);
      }

      if (results.ip >= req.app.config.loginAttempts.forIp || results.ipUser >= req.app.config.loginAttempts.forIpAndUser) {
        workflow.outcome.errors.push('You\'ve reached the maximum number of login attempts. Please try again later.');
        return workflow.emit('response');
      }
      else {
        workflow.emit('attemptLogin');
      }
    };

    require('async').parallel({ ip: getIpCount, ipUser: getIpUserCount }, asyncFinally);
  });

  workflow.on('attemptLogin', function() {
    req._passport.instance.authenticate('local', function(err, user, info) {
      if (err) {
        return workflow.emit('exception', err);
      }

      if (!user) {
        var fieldsToSet = { ip: req.ip, user: req.body.username };
        req.app.db.models.LoginAttempt.create(fieldsToSet, function(err, doc) {
          if (err) {
            return workflow.emit('exception', err);
          }

          workflow.outcome.errors.push('Username and password combination not found or your account is inactive.');
          return workflow.emit('response');
        });
      }
      else {
        req.login(user, function(err) {
          if (err) {
            return workflow.emit('exception', err);
          }

          workflow.emit('saveUserMetaData');
        });
      }
    })(req, res);

  });

  workflow.on('saveUserMetaData', function() {
    var userIp = (req.headers['x-forwarded-for'] || 
                  req.connection.remoteAddress || 
                  req.socket.remoteAddress || 
                  req.connection.socket.remoteAddress);

    var userAgent = req.header('user-agent'),
      $ = {};

    if (/mobile/i.test(userAgent))
      $.Mobile = true;

    if (/like Mac OS X/.test(userAgent)) {
      $.iOS = /CPU( iPhone)? OS ([0-9\._]+) like Mac OS X/.exec(userAgent)[2].replace(/_/g, '.');
      $.iPhone = /iPhone/.test(userAgent);
      $.iPad = /iPad/.test(userAgent);
    }

    if (/Android/.test(userAgent))
      $.Android = /Android ([0-9\.]+)[\);]/.exec(userAgent)[1];

    if (/webOS\//.test(userAgent))
      $.webOS = /webOS\/([0-9\.]+)[\);]/.exec(userAgent)[1];

    if (/(Intel|PPC) Mac OS X/.test(userAgent))
      $.Mac = /(Intel|PPC) Mac OS X ?([0-9\._]*)[\)\;]/.exec(userAgent)[2].replace(/_/g, '.') || true;

    if (/Windows NT/.test(userAgent))
      $.Windows = /Windows NT ([0-9\._]+)[\);]/.exec(userAgent)[1];

    var userLanguage = req.header('accept-language');

    var conditions = {
      _id: req.user._id,
      'metadata.ip': userIp,
      'metadata.agent': userAgent, 
      'metadata.language': userLanguage, 
      'metadata.userCreated.name': req.user.username
    };
    var fieldsToSet = {
      $set: {
        'metadata.$.agent': userAgent, 
        'metadata.$.ip': userIp,
        'metadata.$.language': userLanguage,
      }
    };
    var fieldsToPush = {
      $push: {
        metadata: {
          agent: userAgent,
          ip: userIp,
          language: userLanguage,
          userCreated: {
            id: req.user._id,
            name: req.user.username,
          }
        }
      }
    };
    var options = { upsert: true };

    req.app.db.models.User.findOneAndUpdate(conditions, fieldsToSet, options, function (err, setdocs) {
/*      if (err) {
        console.log(err);
        return workflow.emit('exception', err);
      }*/
      if (setdocs == null) {
        req.app.db.models.User.findByIdAndUpdate(req.user._id, fieldsToPush, function (err, pushdocs) {
          if (err) {
            return workflow.emit('exception', err);
          }
          workflow.emit('saveUserVisit');
        });
      }
      else {
        workflow.emit('saveUserVisit');
      }
    });
  });

  workflow.on('saveUserVisit', function() {
    var fieldsToSet = {
      $push: {
        visits: {
          timeVisited: new Date().toISOString(),
          userCreated: {
            id: req.user._id,
            name: req.user.username,
          }
        }
      }
    };
    var options = { upsert: true, new: true };

    req.app.db.models.User.findByIdAndUpdate(req.user.id, fieldsToSet, options, function(err, loginlogs) {
      if (err) {
        return workflow.emit('exception', err);
      }

      workflow.emit('response');
    });
  });

  workflow.emit('validate');
};

exports.loginTwitter = function(req, res, next){
  req._passport.instance.authenticate('twitter', function(err, user, info) {
    if (!info || !info.profile) {
      return res.redirect('/login/');
    }

    req.app.db.models.User.findOne({ 'twitter.id': info.profile.id }, function(err, user) {
      if (err) {
        return next(err);
      }

      if (!user) {
        res.render('login/index', {
          oauthMessage: 'No users found linked to your Twitter account. You may need to create an account first.',
          oauthTwitter: !!req.app.config.oauth.twitter.key,
          oauthFacebook: !!req.app.config.oauth.facebook.key,
          oauthGoogle: !!req.app.config.oauth.google.key
        });
      }
      else {
        req.login(user, function(err) {
          if (err) {
            return next(err);
          }

          res.redirect(getReturnUrl(req));
        });
      }
    });
  })(req, res, next);
};


exports.loginFacebook = function(req, res, next){
  req._passport.instance.authenticate('facebook', { callbackURL: '/login/facebook/callback/' }, function(err, user, info) {
    if (!info || !info.profile) {
      return res.redirect('/login/');
    }

    req.app.db.models.User.findOne({ 'facebook.id': info.profile.id }, function(err, user) {
      if (err) {
        return next(err);
      }

      if (!user) {
        res.render('login/index', {
          oauthMessage: 'No users found linked to your Facebook account. You may need to create an account first.',
          oauthTwitter: !!req.app.config.oauth.twitter.key,
          oauthFacebook: !!req.app.config.oauth.facebook.key,
          oauthGoogle: !!req.app.config.oauth.google.key
        });
      }
      else {
        req.login(user, function(err) {
          if (err) {
            return next(err);
          }

          res.redirect(getReturnUrl(req));
        });
      }
    });
  })(req, res, next);
};

exports.loginGoogle = function(req, res, next){
  req._passport.instance.authenticate('google', { callbackURL: '/login/google/callback/' }, function(err, user, info) {
    if (!info || !info.profile) {
      return res.redirect('/login/');
    }

    req.app.db.models.User.findOne({ 'google.id': info.profile.id }, function(err, user) {
      if (err) {
        return next(err);
      }

      if (!user) {
        res.render('login/index', {
          oauthMessage: 'No users found linked to your Google account. You may need to create an account first.',
          oauthTwitter: !!req.app.config.oauth.twitter.key,
          oauthFacebook: !!req.app.config.oauth.facebook.key,
          oauthGoogle: !!req.app.config.oauth.google.key
        });
      }
      else {
        req.login(user, function(err) {
          if (err) {
            return next(err);
          }

          res.redirect(getReturnUrl(req));
        });
      }
    });
  })(req, res, next);
};

exports.logout = function(req, res){
  req.logout();
  res.redirect('/');
};