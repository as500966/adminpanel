'use strict';

exports.init = function(req, res){
  if (req.isAuthenticated()) {
    res.redirect(req.user.defaultReturnUrl());
  }
  else {
    res.render('login/reset/index');
  }
};

exports.set = function(req, res){
  var workflow = req.app.utility.workflow(req, res);

  workflow.on('validate', function() {
    if (req.body.password.length < 8) {
      workflow.outcome.errfor.password = 'Must be at least 8 characters';
    }

    if (req.body.confirm.length < 8) {
      workflow.outcome.errfor.password = 'Must be at least 8 characters';
    }

    if (req.body.password !== req.body.confirm) {
      workflow.outcome.errors.push('Passwords do not match.');
    }

    if (workflow.hasErrors()) {
      return workflow.emit('response');
    }

    workflow.emit('findUser');
  });

  workflow.on('findUser', function() {
    var conditions = {
      email: req.params.email,
      resetPasswordExpires: { $gt: Date.now() }
    };
    req.app.db.models.User.findOne(conditions, function(err, user) {
      if (err) {
        return workflow.emit('exception', err);
      }

      if (!user) {
        workflow.outcome.errors.push('Invalid request.');
        return workflow.emit('response');
      }

      req.app.db.models.User.validatePassword(req.params.token, user.resetPasswordToken, function(err, isValid) {
        if (err) {
          return workflow.emit('exception', err);
        }

        if (!isValid) {
          workflow.outcome.errors.push('Invalid request.');
          return workflow.emit('response');
        }

        workflow.emit('patchUser', user);
      });
    });
  });

  workflow.on('patchUser', function(user) {
    req.app.db.models.User.encryptPassword(req.body.password, function(err, hash) {
      if (err) {
        return workflow.emit('exception', err);
      }

      var fieldsToSet = { password: hash, resetPasswordToken: '' };
      var options = { new: true };
      req.app.db.models.User.findByIdAndUpdate(user._id, fieldsToSet, options, function(err, user) {
        if (err) {
          return workflow.emit('exception', err);
        }

        workflow.emit('addAction');
      });
    });

  });

  workflow.on('addAction', function() {
    var conditions = {email: req.params.email};
    var fieldsToSet = {
      $push: {
        actions: {
          data: 'User\'s password has been reset',
          userCreated: {
            id: null,
            name: null,
            time: new Date().toISOString()
          }
        }
      }
    };
    var options = { new: true };
    req.app.db.models.User.findOneAndUpdate(conditions, fieldsToSet, options, function(err, user) {
      if (err) {
        return workflow.emit('exception', err);
      }

      workflow.outcome.user = user;
      return workflow.emit('response');
    });
  });

  workflow.emit('validate');
};
