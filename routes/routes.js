'use strict';

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.set('X-Auth-Required', 'true');
  req.session.returnUrl = req.originalUrl;
  res.redirect('/login/');
}

function ensureAdmin(req, res, next) {
  if (req.user.canPlayRoleOf('admin')) {
    return next();
  }
  res.redirect('/');
}

function ensureAccount(req, res, next) {
  if (req.user.canPlayRoleOf('account')) {
    if (req.app.config.requireAccountVerification) {
      if (req.user.roles.account.isVerified !== 'yes' && !/^\/account\/verification\//.test(req.url)) {
        return res.redirect('/account/verification/');
      }
    }
    return next();
  }
  res.redirect('/');
}

exports = module.exports = function(app, passport) {
  //front end
  app.get('/', require('../routes/index').init);
  app.get('/about/', require('../routes/about/index').init);
  app.get('/contact/', require('../routes/contact/index').init);
  app.post('/contact/', require('../routes/contact/index').sendMessage);


  //sign up
  app.get('/signup/', require('../routes/signup/index').init);
  app.post('/signup/', require('../routes/signup/index').signup);

  //social sign up
  app.post('/signup/social/', require('../routes/signup/index').signupSocial);
  app.get('/signup/twitter/', passport.authenticate('twitter', { callbackURL: '/signup/twitter/callback/' }));
  app.get('/signup/twitter/callback/', require('../routes/signup/index').signupTwitter);
  app.get('/signup/facebook/', passport.authenticate('facebook', { callbackURL: '/signup/facebook/callback/', scope: ['email'] }));
  app.get('/signup/facebook/callback/', require('../routes/signup/index').signupFacebook);
  app.get('/signup/google/', passport.authenticate('google', { callbackURL: '/signup/google/callback/', scope: ['profile email'] }));
  app.get('/signup/google/callback/', require('../routes/signup/index').signupGoogle);

  //login/out
  app.get('/login/', require('../routes/login/index').init);
  app.post('/login/', require('../routes/login/index').login);
  app.get('/login/forgot/', require('../routes/login/forgot/index').init);
  app.post('/login/forgot/', require('../routes/login/forgot/index').send);
  app.get('/login/reset/', require('../routes/login/reset/index').init);
  app.get('/login/reset/:email/:token/', require('../routes/login/reset/index').init);
  app.put('/login/reset/:email/:token/', require('../routes/login/reset/index').set);
  app.get('/logout/', require('../routes/login/index').logout);

  //social login
  app.get('/login/twitter/', passport.authenticate('twitter', { callbackURL: '/login/twitter/callback/' }));
  app.get('/login/twitter/callback/', require('../routes/login/index').loginTwitter);
  app.get('/login/facebook/', passport.authenticate('facebook', { callbackURL: '/login/facebook/callback/' }));
  app.get('/login/facebook/callback/', require('../routes/login/index').loginFacebook);
  app.get('/login/google/', passport.authenticate('google', { callbackURL: '/login/google/callback/', scope: ['profile email'] }));
  app.get('/login/google/callback/', require('../routes/login/index').loginGoogle);

  //admin
  app.all('/admin*', ensureAuthenticated);
  app.all('/admin*', ensureAdmin);

  //admin > home
  app.get('/admin/', require('../routes/admin/index').init);
  app.get('/admin/user-activity/:id', require('../routes/admin/index').activity);
  app.get('/admin/search/', require('../routes/admin/index').find);

  //admin > users
  app.get('/admin/users/', require('../routes/admin/users/index').find);
  app.post('/admin/users/', require('../routes/admin/users/index').create);
  app.get('/admin/users/:id/', require('../routes/admin/users/index').read);
  app.put('/admin/users/:id/', require('../routes/admin/users/index').update);
  app.put('/admin/users/:id/password/', require('../routes/admin/users/index').password);
  app.put('/admin/users/:id/role-admin/', require('../routes/admin/users/index').linkAdmin);
  app.delete('/admin/users/:id/role-admin/', require('../routes/admin/users/index').unlinkAdmin);
  app.put('/admin/users/:id/role-account/', require('../routes/admin/users/index').linkAccount);
  app.delete('/admin/users/:id/role-account/', require('../routes/admin/users/index').unlinkAccount);
  app.post('/admin/users/:id/notes/', require('../routes/admin/users/index').newNote);
  app.delete('/admin/users/:userid/notes/:noteid', require('../routes/admin/users/index').deleteNote);
  app.delete('/admin/users/:id/', require('../routes/admin/users/index').delete);

  //admin > accounts
  app.get('/admin/accounts/', require('../routes/admin/accounts/index').find);
  app.post('/admin/accounts/', require('../routes/admin/accounts/index').create);
  app.get('/admin/accounts/:id/', require('../routes/admin/accounts/index').read);
  app.put('/admin/accounts/:id/', require('../routes/admin/accounts/index').update);
  app.put('/admin/accounts/:id/groups/', require('../routes/admin/accounts/index').groups);
  app.put('/admin/accounts/:id/user/', require('../routes/admin/accounts/index').linkUser);
  app.delete('/admin/accounts/:id/user/', require('../routes/admin/accounts/index').unlinkUser);
  app.delete('/admin/accounts/:id/', require('../routes/admin/accounts/index').delete);

  //admin > administrators
  app.get('/admin/administrators/', require('../routes/admin/administrators/index').find);
  app.post('/admin/administrators/', require('../routes/admin/administrators/index').create);
  app.get('/admin/administrators/:id/', require('../routes/admin/administrators/index').read);
  app.put('/admin/administrators/:id/', require('../routes/admin/administrators/index').update);
  app.put('/admin/administrators/:id/groups/', require('../routes/admin/administrators/index').groups);
  app.put('/admin/administrators/:id/user/', require('../routes/admin/administrators/index').linkUser);
  app.delete('/admin/administrators/:id/user/', require('../routes/admin/administrators/index').unlinkUser);
  app.delete('/admin/administrators/:id/', require('../routes/admin/administrators/index').delete);

  //admin > admin groups
  app.get('/admin/admin-groups/', require('../routes/admin/admin-groups/index').find);
  app.post('/admin/admin-groups/', require('../routes/admin/admin-groups/index').create);
  app.get('/admin/admin-groups/:id/', require('../routes/admin/admin-groups/index').read);
  app.put('/admin/admin-groups/:id/', require('../routes/admin/admin-groups/index').update);
  app.put('/admin/admin-groups/:id/permissions/', require('../routes/admin/admin-groups/index').permissions);
  app.delete('/admin/admin-groups/:id/', require('../routes/admin/admin-groups/index').delete);

//admin > account groups
  app.get('/admin/account-groups/', require('../routes/admin/account-groups/index').find);
  app.post('/admin/account-groups/', require('../routes/admin/account-groups/index').create);
  app.get('/admin/account-groups/:id/', require('../routes/admin/account-groups/index').read);
  app.put('/admin/account-groups/:id/', require('../routes/admin/account-groups/index').update);
  app.put('/admin/account-groups/:id/permissions/', require('../routes/admin/account-groups/index').permissions);
  app.delete('/admin/account-groups/:id/', require('../routes/admin/account-groups/index').delete);

  //admin > statuses
  app.get('/admin/statuses/', require('../routes/admin/statuses/index').find);
  app.post('/admin/statuses/', require('../routes/admin/statuses/index').create);
  app.get('/admin/statuses/:id/', require('../routes/admin/statuses/index').read);
  app.put('/admin/statuses/:id/', require('../routes/admin/statuses/index').update);
  app.delete('/admin/statuses/:id/', require('../routes/admin/statuses/index').delete);

  //admin > categories
  app.get('/admin/categories/', require('../routes/admin/categories/index').find);
  app.post('/admin/categories/', require('../routes/admin/categories/index').create);
  app.get('/admin/categories/:id/', require('../routes/admin/categories/index').read);
  app.put('/admin/categories/:id/', require('../routes/admin/categories/index').update);
  app.delete('/admin/categories/:id/', require('../routes/admin/categories/index').delete);


  //account
  app.all('/account*', ensureAuthenticated);
  app.all('/account*', ensureAccount);

  app.get('/account/', require('../routes/account/index').init);

  //account > verification
  app.get('/account/verification/', require('../routes/account/verification/index').init);
  app.post('/account/verification/', require('../routes/account/verification/index').resendVerification);
  app.get('/account/verification/:token/', require('../routes/account/verification/index').verify);

  //account > settings
  app.get('/account/settings/', require('../routes/account/settings/index').init);
  app.put('/account/settings/', require('../routes/account/settings/index').details);

  //account > settings > social
  app.get('/account/settings/twitter/', passport.authenticate('twitter', { callbackURL: '/account/settings/twitter/callback/' }));
  app.get('/account/settings/twitter/callback/', require('../routes/account/settings/index').connectTwitter);
  app.get('/account/settings/twitter/disconnect/', require('../routes/account/settings/index').disconnectTwitter);
  app.get('/account/settings/facebook/', passport.authenticate('facebook', { callbackURL: '/account/settings/facebook/callback/' }));
  app.get('/account/settings/facebook/callback/', require('../routes/account/settings/index').connectFacebook);
  app.get('/account/settings/facebook/disconnect/', require('../routes/account/settings/index').disconnectFacebook);
  app.get('/account/settings/google/', passport.authenticate('google', { callbackURL: '/account/settings/google/callback/', scope: ['profile email'] }));
  app.get('/account/settings/google/callback/', require('../routes/account/settings/index').connectGoogle);
  app.get('/account/settings/google/disconnect/', require('../routes/account/settings/index').disconnectGoogle);


  //route not found
  app.all('*', require('../routes/http/index').http404);
};
