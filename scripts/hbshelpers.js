Handlebars.registerHelper({
  //2 variables and condition Helper
  ifCond: function (v1, operator, v2, options) {
    switch (operator) {
      //{{#ifCond var1 '==' var2}}
    case '==':
      return (v1 == v2) ? options.fn(this) : options.inverse(this);
    
    //{{#ifCond var1 '===' var2}}
    case '===':
      return (v1 === v2) ? options.fn(this) : options.inverse(this);
    
    //{{#ifCond var1 '!=' var2}}
    case '!=':
      return (v1 != v2) ? options.fn(this) : options.inverse(this);
    
    //{{#ifCond var1 '!==' var2}}
    case '!==':
      return (v1 !== v2) ? options.fn(this) : options.inverse(this);
    
    //{{#ifCond var1 '<' var2}}
    case '<':
      return (v1 < v2) ? options.fn(this) : options.inverse(this);
    
    //{{#ifCond var1 '<=' var2}}
    case '<=':
      return (v1 <= v2) ? options.fn(this) : options.inverse(this);
    
    //{{#ifCond var1 '>' var2}}
    case '>':
      return (v1 > v2) ? options.fn(this) : options.inverse(this);
    
    //{{#ifCond var1 '>=' var2}}
    case '>=':
      return (v1 >= v2) ? options.fn(this) : options.inverse(this);
    
    //{{#ifCond var1 '&&' var2}}
    case '&&':
      return (v1 && v2) ? options.fn(this) : options.inverse(this);
    
    //{{#ifCond var1 '||' var2}}
    case '||':
      return (v1 || v2) ? options.fn(this) : options.inverse(this);
    
    default:
      return options.inverse(this);
    }
  }
  //another helpers
//bar: function() {
//}
});