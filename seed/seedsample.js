/*
---------------------------------------------------------------------------
Это одноразовый генератор тестовых данных для БД чтобы не вводить даные через
консоль. Запускается скрипт через node seedsample.js и предает данные, 
после чего отключает связь с mongodb
---------------------------------------------------------------------------
*/

/*Сюда копируются уже созданные модели из папки models*/
var User = require('../models/user');

const mongoose = require('mongoose');
mongoose.connect('mongodb://192.168.33.10:27017/testdb');

var users = [
  new User({
    email: 'test1@gmail.com',
    password: '11111111'
  }),
  new User({
    email: 'test@gmail.com',
    password: '22222222'
  }),
  new User({
    email: 'test3@gmail.com',
    password: '33333333'
  }),
  new User({
    email: 'test4@gmail.com',
    password: '44444444'
  }),
  new User({
    email: 'test5@gmail.com',
    password: '55555555'
  }),
  new User({
    email: 'test6@gmail.com',
    password: '66666666'
  }),
  new User({
    email: 'test7@gmail.com',
    password: '77777777'
  }),
  new User({
    email: 'test8@gmail.com',
    password: '88888888'
  }),
  new User({
    email: 'test9@gmail.com',
    password: '99999999'
  })
];

var done = 0;
for (var i = 0; i < users.length; i++) {
  users[i].save(function (err, result) {
    done++;
    if (done === users.length) {
      exit();
    }

  });
}

function exit() {
  mongoose.disconnect();
}