'use strict';
//seo-friendly-slug-url
exports = module.exports = function(text) {
  return text.toLowerCase()  //Convert the whole string to lowercase
  .replace(/[^\w ]+/g,'')    //Remove all the non-word characters
  .replace(/ +/g,'-')        //Replace white spaces with hyphens
  .replace(/^-/g,'')         //Remove leading hyphens (caused by leading spaces)
  .replace(/-$/g,'');      //Remove trailing hyphens (caused by trailing spaces)
};

 